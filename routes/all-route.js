// Generated by CoffeeScript 1.7.1
(function() {
  var config, controllers, log, requireTree;

  log = require("helpers/winston-wrapper")(module);

  config = require("nconf");

  requireTree = require('require-tree');

  controllers = requireTree('../controllers');

  module.exports = function() {
    return this.get("/", controllers.render('index'));
  };

}).call(this);

//# sourceMappingURL=all-route.map
