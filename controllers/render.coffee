#--( DEPENDENCIES

log          = require("helpers/winston-wrapper")(module)
config       = require "nconf"
_            = require "underscore"

#--) DEPENDENCIES

module.exports = (template, variables)->
  (req,res)->
    variables ||= {}
    variables['req'] = req

    res.render template, variables


