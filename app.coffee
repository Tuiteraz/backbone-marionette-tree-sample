#--( DEPENDENCIES
log          = require("helpers/winston-wrapper")(module)
config       = require "nconf"
express      = require "express"
bootable     = require "bootable"
bootableEnv  = require "bootable-environment"

#--) DEPENDENCIES

#--- BODY --------------

app = bootable express()

app.phase bootable.initializers('setup/initializers/')
app.phase bootableEnv('setup/environments/',app)
app.phase bootable.routes('routes/all-route.js',app)

app.boot (err)->
  throw err if err
  app.listen config.get('express:port'), ()->
    log.info "Express listen port", config.get("express:port")