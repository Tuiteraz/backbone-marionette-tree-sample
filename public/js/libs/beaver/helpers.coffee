define [
  'libs/beaver/templates'
  'libs/beaver/configuration/beaver-cfg'
  'libs/beaver/helpers/dialog-helpers'
  'libs/beaver/helpers/j'
],(
  tmpl
  cfgBeaver
  dialog_hlprs
) ->

  # will trigger label > i.icon-check|icon-check-empty on hidden input change state
  # sSelector - input
  # инпут должен находится в контейнере div, который содержит { input label i.icon*}
  # +2013.9.10 tuiteraz
  # *2014.1.14 tuiteraz : ^FA 3.2.1
  window.enable_fa_checkbox_for = (sSelector='', hOptions=null) ->
    sCheckClass = "fa-check-square-o"
    sEmptyClass = "fa-square-o"

    #enable i click to fire sSelector 'change' event
    sIconSel = "#{sSelector} + label + div i"
    $(document).delegate "#{sIconSel}","click", (e)->
      jFormGroup = $(this).parents(".form-group")
      jInp = jFormGroup.children("input[type='checkbox']")
      $(jInp)[0].checked = !$(jInp)[0].checked
      jInp.trigger 'change'

    $(document).delegate "#{sSelector}","change", (e)->
      # don't work in ie8

      # inside label case
      jIcon = $(this).parent().children("label").children("i")
      if jIcon.length == 0
        # twbp case : form-group > input:hidden + label + div > i
        jFromGoup = $(this).parents ".form-group"
        jIcon = jFromGoup.contents(".control-label").children("i")

      if $(this)[0].checked
        jIcon.addClass sCheckClass
        jIcon.removeClass sEmptyClass
      else
        jIcon.removeClass sCheckClass
        jIcon.addClass sEmptyClass

    if is_ie 8
      $(sSelector).parent().children("label").click (e)->
        jInput = $(this).parent().children('input')
        jInput[0].checked = !jInput[0].checked

        jIcon = $(this).parent().children("label").children("i")
        if jInput[0].checked
          jIcon.addClass sCheckClass
          jIcon.removeClass sEmptyClass
        else
          jIcon.removeClass sCheckClass
          jIcon.addClass sEmptyClass

  # трансформирум <select> через chosen.js
  # sSelector - определяющий контекст селектор
  # hOptions - хэш опций:
  #   allow_single_deselect: true
  #   no_results_text: "No results matched"
  # +2013.3.7 tuiteraz
  # *2013.4.3 tuiteraz: Отключил временно потому что в контрольной панели глючит при выборе конфы
  window.enable_chosen_select_for = (sSelector='', hOptions=null) ->
    hOptions ||= {}
    hOptions.no_results_text = "Ничего не выбрано" if !defined hOptions.no_results_text
    hOptions.allow_single_deselect = true if !defined hOptions.allow_single_deselect
    #$("#{sSelector} select").chosen hOptions

  # запуск слимскрола к селектору с дополнительными настройками
  # - iHeight - фиксированная высота элемента, но если передается Hash,
  #   то предполагается, что в нем сустановлено свойство height и тогда
  #   hOptions =  iHeight
  # - hOptions - опции слимскорла:
  #   width - Width in pixels of the visible scroll area. Stretch-to-parent if not set. Default: none
  #   height - Height in pixels of the visible scroll area. Also supports auto to set the height to same as parent container. Default: 250px
  #   size - Width in pixels of the scrollbar. Default: 7px
  #   position - left or right. Sets the position of the scrollbar. Default: right
  #   color - Color in hex of the scrollbar. Default: #000000
  #   alwaysVisible - Disables scrollbar hide. Default: false
  #   distance - Distance in pixels from the edge of the parent element where scrollbar should appear. It is used together with position property. Default:1px
  #   start - top or bottom or $(selector) - defines initial position of the scrollbar. When set to bottom it automatically scrolls to the bottom of the scrollable container. When HTML element is passed, slimScroll defaults to offsetTop of this element. Default: top.
  #   wheelStep - Integer value for mouse wheel delta. Default: 20
  #   railVisible - Enables scrollbar rail. Default: false
  #   railColor - Sets scrollbar rail color, Default: #333333
  #   railOpacity - Sets scrollbar rail opacity. Default: 0.2
  #   allowPageScroll - Checks if mouse wheel should scroll page when bar reaches top or bottom of the container. When set to true is scrolls the page.Default: false
  #   scrollTo - Jumps to the specified scroll value. Can be called on any element with slimScroll already enabled. Example: $(element).slimScroll({ scrollTo: '50px' });
  #   scrollBy - Increases/decreases current scroll value by specified amount (positive or negative). Can be called on any element with slimScroll already enabled. Example: $(element).slimScroll({ scrollBy: '60px' });
  #   disableFadeOut - Disables scrollbar auto fade. When set to true scrollbar doesn't disappear after some time when mouse is over the slimscroll div.Default: false
  #   touchScrollStep  - Allows to set different sensitivity for touch scroll events. Negative number inverts scroll direction.Default: 200#
  # +2013.3.10 tuiteraz
  # +2013.4.7 tuiteraz: hOptions.sMarginCSS from caller
  window.enable_slimscroll_for = (sSelector, iHeight, hOptions=null) ->
    if _.isObject iHeight
      hOptions = iHeight
      sHeight = if defined hOptions.height then hOptions.height else "108px"
    else
      sHeight = "#{iHeight}px"

    hOptions ||= {}
    hOptions.railVisible ||= true
    hOptions.height ||= sHeight
    hOptions.start ||= "top"
    sMarginCSS = hOptions.sMarginCSS if defined hOptions.sMarginCSS

    iHeight = parseFloat sHeight
    iBottom = parseFloat($(sSelector).css('padding-bottom')) + parseFloat($(sSelector).css('margin-bottom'))
    sWidth = $(sSelector).css('width')
    if $(sSelector)[0].tagName == "TEXTAREA"
      sNewHeight = "#{iHeight}px"
      sResizeCSS = 'none'
      sMarginCSS = ""
    else
      sNewHeight = "#{iHeight - iBottom - 2}px"
      sResizeCSS = ''
      sMarginCSS = ''


    $(sSelector).slimScroll hOptions
    $(sSelector).css {
      height: sNewHeight # компенсация margin & padding
      overflow: "hidden !important"
      width: sWidth + " !important"
      resize: sResizeCSS
      margin: sMarginCSS
    }

    $(sSelector).mousewheel ->
      hide_all_tooltips()
    $(".slimScrollBar").on {
      drag: ->
        hide_all_tooltips()
    }

  window.get_y_offset = ->
    if typeof(window.pageYOffset)=='number'
      iPageY=window.pageYOffset
    else
      iPageY=document.documentElement.scrollTop

    return iPageY


# после изменения содержимого селекта обновляем chosen.js для него
  # +2013.3.7 tuiteraz
  window.update_chosen_select_for = (Selector) ->
    $(Selector).trigger "liszt:updated" # не забываем обновить chosen.js

  # фокусируемся на первом инпуте в форме
  # +2013.1.18 tuiteraz
  window.autofocus_for = (sSelector) ->
    sSelectorInput = "#{sSelector} input:enabled:visible:not([readonly]):first"
    $(sSelectorInput).focus()
    if !$(sSelectorInput).is ':focus'
      sSelectorText = "#{sSelector} textarea:enabled:visible:not([readonly]):first"
      $(sSelectorText).focus()

  # +2013.1.12 tuiteraz
  window.GUID = ->
    f = (c) ->
      r = Math.random()*16|0
      v = if c == 'x' then r else r&0x3|0x8
      return v.toString(16)
    'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace /[xy]/g, f


  # +2012.11.11 Tuiteraz
  window.defined = (val) ->
    if typeof(val) == 'undefined'
      res =  false
    else if typeof(val) == 'string' and val.length==0
      res =  false
    else if typeof(val) == "object"
      res = !_.isNull val
    else
      res = true

    return res

  # для ссылок <a>|<button> заключенных в ul > li проверяем класс родительского li
  # если disabled, then return true, else return false
  # +2013.2.7 tuiteraz
  window.nav_item_disabled = (sAction) ->
    sSelector = "li>[data-action-id='#{sAction}']:first"
    $(sSelector).parent().hasClass 'disabled'

  # скроем все кнопки которые расположены в заголовках групп аккордиона
  #  вызывается при клике на группу, чтобы скрыть все остальные кнокпи
  # которые должны быть видимы только при активной группе
  # sSelector - селектор корнегого div аккордиона, кнопки которого обрабатываем
  # +2013.2.16 tuiteraz
  # *2013.2.18 tuiteraz: +sSelector
  window.hide_all_accordion_header_btns = (sSelector) ->
    $("#{sSelector} .accordion-heading a.btn").fadeOut()

  # подсветить спец классов активную группу аккордиона
  # sAccId - id аккордиона без '#'
  # +2013.2.28 tuiteraz
  window.colorize_curr_accordion_group = (sAccId) ->
    # добавим активной групппе класс колоризации
    # у всех остальных уберем его
    sSelector1 = "##{sAccId} .accordion-body:not(.in)"
    sSelector2 = "##{sAccId} .accordion-body.in"
    $(sSelector1).parent().removeClass 'accordion-in'
    $(sSelector2).parent().addClass 'accordion-in'



  # обертка для отправки сообщения объекту
  # продолжение выполнения прерывается до заверешения отработки реакции
  # *2012.12.12 Tuiteraz
  window.send_event_to = (oiObj, hDetails, sType='click') ->
    $(oiObj).trigger sType, hDetails


  # удалить привязку тултипов звёздочек обязательных полей
  # +2012.11.13 Tuiteraz
  # *2012.12.12 Tuiteraz
  window.destroy_tooltip_for_required = ->
    jReq = $('.required-field-marker')
    jReq.tooltip 'destroy' if defined jReq

  # включить поддержку подсказки длины введенного текста для всех показанных инуптов и текста
  # +2013.1.22 tuiteraz
  # *2013.3.7 tuiteraz: exclude chosen.js input search
  window.enable_length_tooltip = ->
    render_tooltip= ->
      sHtml = """
        <div id="#{LENGTH_TOOLTIP.id}">
          <div class="content" ></div>
          <div class="arrow" ></div>
        </div>
      """
      $("body").append sHtml


    get_tooltip = ->
      jRes = $("##{LENGTH_TOOLTIP.id}")
      if jRes.length==0
        render_tooltip()
        jRes = $("##{LENGTH_TOOLTIP.id}")
      return jRes

    hide_length_tooltip = ->
      jLT = get_tooltip()
      jLT.fadeOut 'fast'

    show_length_tooltip = (jInput, bNoRefresh=false) ->
      clearTimeout window.juicy_length_tooltip_timeout

      #console.enable_log "enable_length_tooltip(show_length_tooltip)"
      jLT = get_tooltip()
      #console.log "jLT=%s",jLT
      hCoord = j.get_coordinates_of jInput

      jLT.css {
        left: hCoord.iWidth + hCoord.iLeft - 10
        top: hCoord.iTop - 30
      }

      iTextSize = $(jInput).attr('value').length
      jLT.children(".content").text iTextSize
      #console.log "$(jInput).attr('value')=%s",$(jInput).attr('value')

      jLT.fadeIn 'slow'
      window.juicy_length_tooltip_timeout = setTimeout =>
        jLT.fadeOut 'slow'
      ,LENGTH_TOOLTIP.inactive_timeout

      if !bNoRefresh
        window.juicy_length_tooltip_refresh_timeout = setTimeout =>
          show_length_tooltip(jInput,true)
        , 100

    sCheckBox = ":not([type='#{FRM.input.type.checkbox}'])"
    sNum      = ":not([type='#{FRM.input.type.number}'])"
    sReadOnly = ":not([readonly='readonly'])"
    sChzInp = ":not([autocomplete])"
    sInpSel = "input#{sCheckBox}#{sNum}#{sReadOnly}#{sChzInp}"

    $("ul.nav li").unbind('click').on {
      click: ->
        hide_length_tooltip()
    }

    $(sInpSel).on {
      keypress: (e) ->
        show_length_tooltip(this) if !(e.keyCode==9)  #!tab

      focus: ->
        setTimeout =>
          show_length_tooltip(this)
        , LENGTH_TOOLTIP.after_focus_timeout
      blur: ->
        hide_length_tooltip()
    }

    $("textarea").on {
    keypress: (e) ->
      show_length_tooltip(this) if !(e.keyCode==9)  # !tab

    focus: ->
      setTimeout =>
        show_length_tooltip(this)
      , LENGTH_TOOLTIP.after_focus_timeout
    blur: ->
      hide_length_tooltip()

    }
    $(document).scroll ->
      hide_length_tooltip()



  # +2012.11.11 Tuiteraz
  # *2013.6.1 Tuiteraz: + sTooltip , +iShowTime
  window.enable_tooltip_for_required = (sTooltip='',iShowTime=2500) ->
    sTooltip = "Обязательно для заполнения" if !defined sTooltip
    $("a.required-field-marker[data-original-title]").attr "data-original-title", sTooltip
    $('.required-field-marker').tooltip {
    placement: "top"
    delay: {show: iShowTime, hide: 100}
    }
    $(".required-field-marker").unbind('click').click (e)->
      e.preventDefault()


  # удалить привязку тултипов кнопок
  # +2012.11.13 tuiteraz
  window.destroy_tooltip_for_btn = ->
    #j_btn = $('.btn')
    #j_btn.tooltip 'destroy' if defined j_btn
    $("div.tooltip").remove()

  # +2012.11.11 Tuiteraz
  # *2012.11.13 Tuiteraz
  # *2012.12.12 Tuiteraz
  window.enable_tooltip_for_btn = (sSelector='') ->
    #console.enable_log "enable_tooltip_for_btn()"
    #console.log "sSelector=#{sSelector}"
    $("#{sSelector}").tooltip {
    placement: "top"
    delay: {show: 2500, hide: 100}
    }

  # +2012.11.15 Tuiteraz
  # *2012.12.12 Tuiteraz
  # *2013.05.30 Tuiteraz: +iShowTime, iHideTime
  window.enable_tooltip_for = (sSelector='',iShowTime=2500, iHideTime=100) ->
    $("#{sSelector}").tooltip {
    placement: "bottom"
    delay: {show: iShowTime, iHideTime: 100}
    }

  # включаем для инпутов всплывающуюу подсказу снизу при наведении
  # если она была прорисована
  # Вызывать нужно после обновления данных формы для привязки реакций
  # sSelector - путь к диву формы/закладки для которой нужна активация функции
  #             уже включает символ #
  # +2013.1.18 tuiteraz
  window.enable_focusin_help_for = (sSelector) ->
    hlpr =
      on_focus: (jObj) ->
        jParent = $(jObj).parent()
        jHelpBlock = $(jParent).children(".help-block:first")
        jHelpBlock.slideDown()
      on_blur: (jObj) ->
        jParent = $(jObj).parent()
        jHelpBlock = $(jParent).children(".help-block:first")
        jHelpBlock.slideUp()


    sSelector += " .controls"
    $("#{sSelector} input").on {
      blur: ->
        hlpr.on_blur this
      focus: ->
        hlpr.on_focus this
    }

    $("#{sSelector} textarea").on {
    blur: ->
      hlpr.on_blur this
    focus: ->
      hlpr.on_focus this
    }


  # скрыть тултипы размера и обычные
  # !2012.12.12 Tuiteraz
  # *2013.3.11 Tuiteraz
  # *2013.4.3 Tuiteraz: * tooltip hide вместо tooltip.hide 'tooltip'
  window.hide_all_tooltips = ->
    #$(".tooltip").tooltip 'hide'
    $(".tooltip").hide()
    jLT = $("##{LENGTH_TOOLTIP.id}")
    jLT.fadeOut 'fast' if jLT.length >= 0

  # поддержка кнопок спиннера для ввода чисел
  # +2012.11.11 Tuiteraz
  # *2012.12.12 Tuiteraz
  # *2013.1.25 Tuiteraz: не должен работать на неактивном инпуте
  # *2013.1.30 Tuiteraz: нагружает проц интервальная проверка
  # *2013.2.11 Tuiteraz: $(jInput).trigger 'change' для отслеживания в модулях контекста
  window.enable_twbp_spinner = ->
    #jconsole.enable_log "enable_twbp_spinner()"
    #console.log "defined window.iSpinnerCheckIntervalId=%s",defined window.iSpinnerCheckIntervalId
    # вешаем проверку доступности инпута к которому привязан спиннер
    # чтобы активировать или деактивироть его кнопки
    check_spinners = ->
      #console.enable_log "enable_twbp_spinner(interval)"
      oiTime = new Date()
      sTime = "#{oiTime.getMinutes()}:#{oiTime.getSeconds()}"
      #console.log "(#{sTime})checking.."
      aSpinnerBtns = $(".spinner-btn-up:visible")
      #console.log "[#{aSpinnerBtns.length}] spinner up btns found"
      for jSpinnerBtn in aSpinnerBtns
        sInpId = $(jSpinnerBtn).attr "data-target-input-id"
        #console.log "input id = #{sInpId}"
        jInput = $("##{sInpId}")
        if jInput.hasClass('disabled') or jInput.hasAttr('readonly')
          $(jSpinnerBtn).addClass 'disabled'
        else
          $(jSpinnerBtn).removeClass 'disabled'

      aSpinnerBtns = $(".spinner-btn-down:visible")
      for jSpinnerBtn in aSpinnerBtns
        sInpId = $(jSpinnerBtn).attr "data-target-input-id"
        jInput = $("##{sInpId}")
        if jInput.hasClass('disabled') or jInput.hasAttr('readonly')
          $(jSpinnerBtn).addClass 'disabled'
        else
          $(jSpinnerBtn).removeClass 'disabled'

    if !defined window.iSpinnerCheckIntervalId
      #console.log "setting interval execution..."
      window.iSpinnerCheckIntervalId = setInterval =>
        check_spinners()
      , DATAVIEW.spinner_btn_refresh_state_time

    # вешаем обработчик кликов
    $(".spinner-btn-up").unbind('click').on {
    click:(e) ->
      #jconsole.enable_log "enable_twbp_spinner(up click)"
      jInput = $(this).parent().parent().children("input:first")
      if !jInput.hasClass('disabled') && !jInput.hasAttr('readonly')
        e.preventDefault()
        e.stopPropagation()
        sInputId = $(this).attr 'data-target-input-id'
        jInput = $("##{sInputId}")
        iCurrValue = parseInt $(jInput).val()
        iCurrValue = if isNaN(iCurrValue) then 0 else iCurrValue
        iCurrValue += 1
        $(jInput).val(iCurrValue.toString())
        $(jInput).trigger 'change'
      else
        e.preventDefault()
    }

    $(".spinner-btn-down").unbind('click').on {
    click:(e) ->
      jInput = $(this).parent().parent().children("input:first")
      if !jInput.hasClass('disabled') && !jInput.hasAttr('readonly')
        e.preventDefault()
        e.stopPropagation()

        sInputId = $(this).attr 'data-target-input-id'
        jInput = $("##{sInputId}")
        iCurrValue = parseInt $(jInput).val()
        iCurrValue = if isNaN(iCurrValue) then 0 else iCurrValue
        iCurrValue -= 1
        $(jInput).val(iCurrValue.toString())
        $(jInput).trigger 'change'
      else
        e.preventDefault()

    }

  init:->
    @hCfg = cfgBeaver.get_content()

    @dialog = dialog_hlprs



