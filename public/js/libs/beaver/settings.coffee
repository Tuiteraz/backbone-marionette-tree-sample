# !2012.12.26 tuiteraz
# *2013.3.17 tuiteraz
window.FRM=
  input:
    style:
      input    : 'input'
      image    : 'image'
      text     : 'textarea'
      radio_v  : 'radio-vertical'
      radio_h  : 'radio-horizontal'
      select   : 'select'
      checkbox : 'checkbox'
      file     : 'file'
    type:
      hidden: 'hidden'
      text: 'text'
      number: 'number'
      checkbox: 'checkbox'
      password: 'password'
  list:
    action:
      view:
        id:      'view-item'
        icon:    'eye-open'
        tooltip: 'Открыть элемент'
      destroy:
        id:      'destroy-item'
        icon:    'trash'
        tooltip: 'Удалить элемент'

window.LENGTH_TOOLTIP=
  id: "juicy-input-length-tooltip"
  inactive_timeout: 5000 # время неактивности инпута после которого скрывается тултип
  after_focus_timeout: 1000 #  время задержки после фокуса на инпут, чтобы показать тултип





