define [
  'beaver-console'
  'libs/beaver/templates'
  'libs/beaver/helpers'
  'underscore'
],(jconsole,tmpl,hlprs) ->
  window.j =

    # sort helper for multidimensional object array sorting
    # sKey - clean prop name without +|-
    #+2014.2.18 tuiteraz
    by : (sKey,fnMinor)->
      return (o,p)->
        if _.isObject(o) && _.isObject(p) && o && p
          a = j.hash.get_deep_key_value o,sKey
          b = j.hash.get_deep_key_value p,sKey
          if (a == b)
            return if _.isFunction(fnMinor) then fnMinor(o, p) else o

          if (typeof a == typeof b)
            return if a < b then -1 else 1

          return if typeof a < typeof b then -1 else 1
        else
          throw {
          name: 'Error',
          message: 'Expected an object when sorting by ' + name
          }

    hash:

      # get value of hash by string e.g. "refCollection.i18nTitle"
      #+2014.2.18 tuiteraz
      get_deep_key_value:(hHash,sDeepKey)->
        aKeys = sDeepKey.split '.'
        Cursor = hHash

        for sKey in aKeys
          if !_.isNull(Cursor[sKey]) && !_.isUndefined(Cursor[sKey])
            Cursor = Cursor[sKey]
          else
            return null

        return Cursor

      #+2014.2.22 tuiteraz
      set_deep_key_value:(hHash,sDeepKey,Value)->
        aKeys = sDeepKey.split '.'
        Cursor = hHash
        sLastKey = _.last aKeys
        for sKey in aKeys
          if sKey == sLastKey
            Cursor[sKey] = Value
          else
            Cursor[sKey] ||= {}
            Cursor = Cursor[sKey]

        Cursor = Value

        return hHash

    html:
      # make html-safe from sValue's html
      # sValue - html text
      # +2013.4.30 tuiteraz
      encode: (sValue) ->
        # create a in-memory div, set it's inner text(which jQuery automatically encodes)
        # then grab the encoded contents back out.  The div never exists on the page.
        $('<div/>').text(sValue).html();

      # decode html-safe text to html
      # sValue - html-safe text
      # +2013.4.30 tuiteraz
      decode: (sValue) ->
        $('<div/>').html(sValue).text();

    object:
      attr:

        # получить значение атрибута. Если Attr - строка, то просто вернуть строку.
        # Если объект, то вернуть значение атрибута объекта по его sListViewAttrCode
        # +2013.3.23 tuiteraz
        # *2013.3.24 tuiteraz: +hElement
        #   hElement.fnValue called when _.isFunction hElement.fnValue
        # *2013.3.30 tuiteraz: +bCleanValue - без выполнения функции и иконок вместо true|false
        value: (Attr, hElement=null, bCleanValue=true) ->

          fnValByListViewAttr =  ->
            if _.isObject 'Attr'
              sListViewAttrCode = Attr.hObjectAttributes.sListViewAttrCode
              return Attr.hAttributes[sListViewAttrCode]
            else
              return Attr


          if _.isObject Attr # если Attr - это объект
            if _.isNull hElement
              return fnValByListViewAttr()
            else if defined hElement.fnValue
              if _.isFunction hElement.fnValue
                return hElement.fnValue Attr
              else
                return fnValByListViewAttr()
          else if !_.isNull hElement # если не пустой параметр атрибутов Attr
            if _.isBoolean(Attr) && !bCleanValue
              sIconName = "check"
              sIconName += if Attr then '' else '-empty'
              return "<i class='icon-#{sIconName}' />"
            else
              return fnValByListViewAttr()
          else if _.isBoolean(Attr) && !bCleanValue
            sIconName = "check"
            sIconName += if Attr then '' else '-empty'
            return "<i class='icon-#{sIconName}' />"
          else
            return Attr

    # заполним html форму данными полученными с сервера
    # hElements - ссылка на узел типа CONFMAN.tmpl.conf.consts.form.controls
    #  подразумевается, что все id в elements уникальны для текущего document.body
    # hAttrData - хэш серверных данных монго в виде: название атрибута => значение
    # +2012.11.10 Tuiteraz
    # *2012.12.12 Tuiteraz
    # *2013.1.10 Tuiteraz
    # *2013.3.4 Tuiteraz: если в параметрах не указан жесткий список опций то используем опции из html
    # *2013.3.7 Tuiteraz: call update_chosen_select_For on setting select value
    # *2013.3.20 Tuiteraz: * назначать значение элементу только если есть данные для него
    # *2013.3.22 Tuiteraz: moved from dataview/helpers to dataview/j
    #   + sId - если указана строка, то использовать селектор вида "sId hAttrs.sIdClass"
    # *2013.3.23 tuiteraz: Обработка вывода референсного атрибута, например refUser, который здесь представлен всемисвоими атрибутами
    # *2013.3.30 tuiteraz: выводить пустое значение если значения для атрибута не передано
    set_elements_data: (hElements,hAttrData, sId = null) ->
      jconsole.enable_log "set_elements_data"
      for sName, hAttrs of hElements
        sSelector = if _.isNull sId then "##{hAttrs.id}" else "##{sId} .#{hAttrs.sIdClass}"
        jObj = $(sSelector)
        if jObj.length != 0
          sAttrValue = ''
          sAttrValue = j.object.attr.value(hAttrData[sName],hAttrs) if defined hAttrData[sName]

          if hAttrs.sInputStyle == FRM.input.style.input
            jObj.val sAttrValue
          else if hAttrs.sInputStyle == FRM.input.style.text
            jObj.val sAttrValue
          else if hAttrs.sInputStyle == FRM.input.style.checkbox
            if defined hAttrData[sName]
              jObj[0].checked = sAttrValue
            else
              jObj[0].checked = false
          else if hAttrs.sInputStyle == FRM.input.style.select
            if defined hAttrs.hOptions
              jObj.attr 'value', hAttrs.hOptions[sAttrValue] if defined hAttrData[sName]
            else if $("#{sSelector} option").length > 0
              sCustomOptSelector = "#{sSelector} option[data-opt-link='#{sAttrValue}']"
              if $(sCustomOptSelector).length == 1
                jObj.attr 'value', $(sCustomOptSelector).attr 'value' if defined hAttrData[sName]
              else
                jObj.attr 'value', sAttrValue
            else
              jObj.attr 'value', sAttrValue if defined hAttrData[sName]
            update_chosen_select_for jObj
          else if (hAttrs.sInputStyle == FRM.input.style.radio_v) or (hAttrs.sInputStyle == FRM.input.style.radio_h)
            # сначала удалим все установки существующие
            if defined hAttrData[sName]
              sSelector = if _.isNull sId then "input[name='#{hAttrs.id}']" else "##{sId} input[name='#{hAttrs.sIdClass}']"
              $(sSelector).removeAttr 'checked'
              jObj = $("#{sSelector}[data-opt-code='#{sAttrValue}']")
              jObj.attr 'checked','checked'


    # проверка заполненности данных формы по флагам из настроек атрибута
    # которые копируются в хэш вместе с данными, чтобы не зависеть от узла настроек
    # в котором оригинально были указаны
    # формат:
    # hElemsData[sName]=
    #    Value
    #    sLabel
    #    sFullName
    #    sDataType
    #    bDataRequired
    # +2012.11.10 Tuiteraz
    # *2012.12.12 Tuiteraz: refactoring
    # *2012.12.27 Tuiteraz: refactoring
    # *2013.1.9 Tuiteraz: реальной валидации пока не производится в клиенте
    # *2013.3.21 Tuiteraz: moved to j
    # *2014.2.11 Tuiteraz: renamed to validate_controls_data
    validate_controls_data: (hControls, sParentSelector, fnCallback) ->
      hData = j.get_controls_data(hControls,sParentSelector)

      aCtrlErrors = []
      for sPropName,hAttrs of hData
        # -= REQUIRED =-
        if ( hAttrs.bRequired == true ) and (hAttrs.Value.length == 0) and (sPropName != 'id')
          aCtrlErrors.push {
            sType: 'warn'
            sTitle: hAttrs.sFullName
            sMessage: "Required property is empty [#{hAttrs.sFullName}]"
            sPropName : sPropName
          }

      fnCallback aCtrlErrors, hData

    # устанавливает или получает текущий CSRF для страницы
    # +2013.3.18 tuiteraz
    CSRF: (hData=null) ->
      if defined hData
        $("meta[name='csrf-param']").attr 'content', hData.sParam
        $("meta[name='csrf-token']").attr 'content', hData.sToken
      else
        {
          sParam: $("meta[name='csrf-param']").attr ('content')
          sToken: $("meta[name='csrf-token']").attr ('content')
        }

    # получить координыта и размеры jQuery объекта
    # +2013.1.22 tuiteraz
    get_coordinates_of: (jObj) ->
      hRes =
        iWidth:  $(jObj).width()
        iHeight: $(jObj).height()
        iLeft:   $(jObj).offset().left
        iTop:    $(jObj).offset().top

    # получить данные из html по хэшу элементов, по которому он формировался
    # параметром передается узел ветки CONFMAN
    # +2012.11.10 Tuiteraz
    # *2012.12.12 Tuiteraz: refactoring
    # *2013.3.1 Tuiteraz: refactoring -> get_elements_data()
    # *2013.3.21 Tuiteraz: moved to j
    get_controls_data: (hElements,sParentSelector=null) ->
      hElemsData = {}
      hElemsData[sName]= j.frm.control.get_data(hAttrs,sParentSelector) for sName, hAttrs of hElements
      return hElemsData

    # +2013.3.12 tuiteraz
    parse_bool: (str) ->
      return /^true$/i.test(str)

    # парсим строку JSON с помощью jQuery через исключения
    # +2013.1.11 tuiteraz
    parse_JSON: (sText) ->
      #jconsole.enable_log "j.parse_json"
      try
        if !_.isEmpty(sText)
          jRes = $.parseJSON sText
        else
          jconsole.log "[j.parse_json] sText is empty"
          jRes = null
      catch jErr
        jconsole.log "jErr=%s",jErr
        jRes = {}
      return jRes

    frm :
      # +2013.2.23 tuiteraz
      is_checked: (sId) ->
        jChBox = $("##{sId}")
        if jChBox.length > 0
          bRes = jChBox[0].checked
        else
          bRes = false

        return bRes

      control:

        # +2013.2.23 tuiteraz
        clear: (hControl) ->
          if _.isObject(hControl)
            jObj = $("##{hControl.id}")
            if hControl.sInputStyle == FRM.input.style.input
              jObj.val ''
            else if hControl.sInputStyle == FRM.input.style.text
              jObj.val ''
            else if hControl.sInputStyle == FRM.input.style.checkbox
              jObj[0].checked = false
            else if hControl.sInputStyle == FRM.input.style.select
              jObj.attr 'value', ''
            else if (hControl.sInputStyle == FRM.input.style.radio_v) or (hControl.sInputStyle == FRM.input.style.radio_h)
              # сначала удалим все установки существующие
              $("input[name='#{hAttrs.id}']").removeAttr 'checked'

          else
            jconsole.enable_log "j.clear()"
            jconsole.warn "param is wrong: %s",hControl

        # получить значение из одного элемента
        # +2013.3.1 tuiteraz
        # *2013.3.4 tuiteraz: для списка если нет атрибута data-opt-code, то проверяем data-opt-link, value
        # *2013.3.21 tuiteraz: bDataRequired переносится из hElement
        # *2013.3.25 tuiteraz: + sId = null
        # *2013.6.2 tuiteraz: res << bRequired , sValueType, sElementId
        get_data: (hControl, sParentSelector = null) ->
          sSelector = if !_.isNull sParentSelector then sParentSelector else ""
          if !_.isUndefined(hControl.id)
            sSelector += " ##{hControl.id}"
          else if !_.isUndefined(hControl.sIdClass)
            sSelector += " .#{hControl.sIdClass}"

          jObj = $(sSelector)
          if hControl.sInputStyle == FRM.input.style.input
            sValue = jObj.val()
            sValue = jObj.text() if _.isEmpty(sValue) && (jObj[0].tagName == "SPAN")
          else if hControl.sInputStyle == FRM.input.style.text
            sValue = jObj.val()
          else if hControl.sInputStyle == FRM.input.style.checkbox
            sValue = jObj[0].checked
          else if (hControl.sInputStyle == FRM.input.style.radio_v) or (hControl.sInputStyle == FRM.input.style.radio_h)
            jObj = $("input[name='#{hControl.id}']:checked")
            sValue = jObj.attr 'data-opt-code'
          else if hControl.sInputStyle == FRM.input.style.select
            if jObj[0].tagName == "SELECT"
              iIdx = jObj[0].selectedIndex
              oSelected_option = jObj[0].options[iIdx]
              if $(oSelected_option).hasAttr 'data-opt-code'
                sValue = $(oSelected_option).attr 'data-opt-code'
              else if $(oSelected_option).hasAttr 'data-opt-link'
                sValue = $(oSelected_option).attr 'data-opt-link'
              else
                sValue = $(oSelected_option).attr 'value'

              sValueTitle = oSelected_option.text
            else
              sValue = null

          hRes =
            Value           : sValue
            sValueTitle     : sValueTitle
            sLabel          : hControl.sLabel
            sFullName       : hControl.sFullName
            bRequired       : hControl.bRequired
            sControlId      : hControl.id
            sControlIdClass : hControl.sIdClass

          return hRes

        # проверяет содержит ли переданный эелемент указанное значение
        # +2013.3.1 tuiteraz
        has_value: (hControl,sTrackValue) ->
          hRes = j.frm.control.get_data hControl
          bRes = if hRes.Value == sTrackValue then true else false


        # +2013.2.4 tuiteraz
        hidden: (hControl) ->
          sId = if defined hControl.sCGid then hControl.sCGid else hControl.id
          $("##{sId}:hidden").length == 1

        # +2013.2.23 tuiteraz
        hide: (hControl, bClearAfterHide=false) ->
          if _.isObject(hControl) and defined(hControl.sCGid)
            $("##{hControl.sCGid}").slideUp 'fast', =>
              j.frm.control.clear hControl if bClearAfterHide
          else
            jconsole.enable_log "j.hide()"
            jconsole.warn "param is wrong: %s",hControl

        # +2013.2.23 tuiteraz
        show: (hControl) ->
          if _.isObject(hControl) and defined(hControl.sCGid)
            $("##{hControl.sCGid}").slideDown 'fast'
          else
            jconsole.enable_log "j.show()"
            jconsole.warn "param is wrong: %s",hControl

        # +2013.2.4 tuiteraz
        visible: (hControl) ->
          sId = if defined hControl.sCGid then hControl.sCGid else hControl.id
          $("##{sId}:visible").length == 1

