define [
  'libs/beaver/templates'
  'libs/beaver/configuration/beaver-cfg'
],(
  tmpl
  cfgBeaver
) ->

  #+2014.2.5 tuiteraz
  before_filter: ->
    @hCfgBeaver = cfgBeaver.get_content() if _.isUndefined(@hCfgBeaver)
    @bind_html_events_once = _.once(@bind_html_events) if _.isUndefined(@bind_html_events_once)

  #+2014.2.5 tuiteraz
  alert: (sText='',sTitle='Message') ->
    @fnYes    = null
    @fnNo     = null
    @fnCancel = null

    @before_filter()

    @render_alert(sText,sTitle)
    @bind_html_events_once()

    $("##{@hCfgBeaver.dialog.id}").modal "show"

  # показать диалог с вопросом и кнопками дождаться ответа и вернуть действие из кнопки
  # диалог всегда осздается перед показом, а после ответа удаляется
  # aBtnStyles - [sYesBtnStyle,sNoBtnStyle,sCancelBtnStyle]
  #   sYesBtnStyle - 'success'|'warning'|'danger'|'primary'|'info'|'inverse'
  #   определяет приставку к дополнительному классу типа btn-primary
  # +2013.2.1 tuiteraz
  # *2013.3.12 tuiteraz: +bNoCancelBtn
  ask: (sTitle='',sText='',@fnYes,@fnNo,@fnCancel,bNoCancelBtn=false) ->
    @before_filter()

    @render_ask(sTitle,sText,bNoCancelBtn)
    @bind_html_events_once()

    $("##{@hCfgBeaver.dialog.id}").modal "show"

  #+2014.2.5 tuiteraz
  bind_html_events:() ->
    hOk     = @hCfgBeaver.dialog.buttons.ok
    hYes    = @hCfgBeaver.dialog.buttons.yes
    hNo     = @hCfgBeaver.dialog.buttons.no
    hCancel = @hCfgBeaver.dialog.buttons.cancel

    # BTN FN's
    $(document).delegate "[#{hOk.sActionAttrName}='#{hOk.sAction}']","click", =>
      @hide()

    $(document).delegate "[#{hYes.sActionAttrName}='#{hYes.sAction}']","click", =>
      @hide()
      @fnYes() if _.isFunction @fnYes

    $(document).delegate "[#{hNo.sActionAttrName}='#{hNo.sAction}']","click", =>
      @hide()
      @fnNo() if _.isFunction @fnNo

    $(document).delegate "[#{hCancel.sActionAttrName}='#{hCancel.sAction}']","click", =>
      @hide()
      @fnCancel() if _.isFunction @fnCancel

    # MODAL
    $("##{@hCfgBeaver.dialog.id}").modal {
      backdrop: true
      show: false
    }

    # after hiding need to correct window.location - remove item id
    $("##{@hCfgBeaver.dialog.id}").on "hidden.bs.modal", (e)=>
      @remove_html()

  #+2014.2.5 tuiteraz
  hide: ->
    $("##{@hCfgBeaver.dialog.id}").modal 'hide'

  #+2014.2.5 tuiteraz
  is_html_exist: ->
    $("##{@hCfgBeaver.dialog.id}").length > 0 ? true : false

  #+2014.2.5 tuiteraz
  render_alert: (sText,sTitle) ->
    $("##{@hCfgBeaver.dialog.id}").remove() if @is_html_exist()

    hOk  = @hCfgBeaver.dialog.buttons.ok

    sModalParams = "tabindex='-1' role='dialog' aria-hidden='true'"
    sHtml = tmpl.div "modal fade", @hCfgBeaver.dialog.id,sModalParams, [
      tmpl.div "modal-dialog","","",[
        tmpl.div "modal-content","","",[
          tmpl.div "modal-header", [
            tmpl.button "close pull-right", "", "data-dismiss='modal' aria-hidden='true'", "&times;"
            tmpl.h 3, sTitle,"no-margin"
            tmpl.clearfix()
          ]
          tmpl.div "modal-body", [
            sText
          ]
          tmpl.div "modal-footer", [
            tmpl.icon_button hOk
          ]
        ]
      ]
    ]
    $("body").append sHtml

  #+2014.2.5 tuiteraz
  remove_html: ->
    $("##{@hCfgBeaver.dialog.id}").remove()


  #+2014.2.5 tuiteraz
  render_ask:(sTitle,sText,bNoCancelBtn) ->
    $("##{@hCfgBeaver.dialog.id}").remove() if @is_html_exist()

    hYes     = @hCfgBeaver.dialog.buttons.yes
    hNo      = @hCfgBeaver.dialog.buttons.no
    hCancel  = @hCfgBeaver.dialog.buttons.cancel

    if bNoCancelBtn
      sCancelBtnHtml = ''
    else
      sCancelBtnHtml = tmpl.icon_button hCancel

    sModalParams = "tabindex='-1' role='dialog' aria-hidden='true'"
    sHtml = tmpl.div "modal fade", @hCfgBeaver.dialog.id,sModalParams, [
      tmpl.div "modal-dialog","","",[
        tmpl.div "modal-content","","",[
          tmpl.div "modal-header", [
            tmpl.button "close pull-right", "", "data-dismiss='modal' aria-hidden='true'", "&times;"
            tmpl.h 3, sTitle, "no-margin"
            tmpl.clearfix()
          ]
          tmpl.div "modal-body", [
            sText
          ]
          tmpl.div "modal-footer", [
            tmpl.icon_button hYes
            tmpl.icon_button hNo
            sCancelBtnHtml
          ]
        ]
      ]
    ]

    #console.log "sHtml1=#{sHtml}"
    $("body").append sHtml
    $("##{@hCfgBeaver.dialog.id}").modal "show"
