define ->
  f_enabled = false
  log_context = 'GLOBAL'

  mod = this

  enable_log: (context='GLOBAL') ->
    window.bJvmConsoleEnabled = true
    window.sJvmConsoleLogContext = context

  disable_log: ->
    window.bJvmConsoleEnabled = false
    window.sJvmConsoleLogContext = 'GLOBAL'

  log: (msg,var1=null,var2=null,var3=null) ->

    var1 = JSON.stringify(var1) if var1 != null
    var2 = JSON.stringify(var2) if var2 != null
    var3 = JSON.stringify(var3) if var3 != null

    if var1 != null
      if var2 != null
        if var3 != null
          console.log("[#{sJvmConsoleLogContext}] #{msg}",var1,var2,var3) if bJvmConsoleEnabled
        else
          console.log("[#{sJvmConsoleLogContext}] #{msg}",var1,var2) if bJvmConsoleEnabled
      else
        console.log("[#{sJvmConsoleLogContext}] #{msg}",var1) if bJvmConsoleEnabled
    else
      console.log("[#{sJvmConsoleLogContext}] #{msg}") if bJvmConsoleEnabled

  info: (msg='',var1=null,var2=null,var3=null) ->
    var1 = JSON.stringify(var1) if var1 != null
    var2 = JSON.stringify(var2) if var2 != null
    var3 = JSON.stringify(var3) if var3 != null

    if var1 != null
      if var2 != null
        if var3 != null
          console.info("[#{sJvmConsoleLogContext}] #{msg}",var1,var2,var3) if bJvmConsoleEnabled
        else
          console.info("[#{sJvmConsoleLogContext}] #{msg}",var1,var2) if bJvmConsoleEnabled
      else
        console.info("[#{sJvmConsoleLogContext}] #{msg}",var1) if bJvmConsoleEnabled
    else
      console.info("[#{sJvmConsoleLogContext}] #{msg}") if bJvmConsoleEnabled

  debug: (msg,var1=null,var2=null,var3=null) ->
    var1 = JSON.stringify(var1) if var1 != null
    var2 = JSON.stringify(var2) if var2 != null
    var3 = JSON.stringify(var3) if var3 != null

    if var1 != null
      if var2 != null
        if var3 != null
          if !is_ie()
            console.debug("[DEBUG] [#{sJvmConsoleLogContext}] #{msg}",var1,var2,var3) if bJvmConsoleEnabled
          else
            console.log("[DEBUG] [#{sJvmConsoleLogContext}] #{msg}",var1,var2,var3) if bJvmConsoleEnabled
        else
          if !is_ie()
            console.debug("[DEBUG] [#{sJvmConsoleLogContext}] #{msg}",var1,var2) if bJvmConsoleEnabled
          else
            console.log("[DEBUG] [#{sJvmConsoleLogContext}] #{msg}",var1,var2) if bJvmConsoleEnabled
      else
        if !is_ie()
          console.debug("[DEBUG] [#{sJvmConsoleLogContext}] #{msg}",var1) if bJvmConsoleEnabled
        else
          console.log("[DEBUG] [#{sJvmConsoleLogContext}] #{msg}",var1) if bJvmConsoleEnabled
    else
      if !is_ie()
        console.debug("[DEBUG] [#{sJvmConsoleLogContext}] #{msg}") if bJvmConsoleEnabled
      else
        console.log("[DEBUG] [#{sJvmConsoleLogContext}] #{msg}") if bJvmConsoleEnabled

  #+2013.9.23 tuiteraz
  error: (msg,var1=null,var2=null,var3=null) ->
    var1 = JSON.stringify(var1) if var1 != null
    var2 = JSON.stringify(var2) if var2 != null
    var3 = JSON.stringify(var3) if var3 != null

    if var1 != null
      if var2 != null
        if var3 != null
          console.error("[#{sJvmConsoleLogContext}] #{msg}",var1,var2,var3) if bJvmConsoleEnabled
        else
          console.error("[#{sJvmConsoleLogContext}] #{msg}",var1,var2) if bJvmConsoleEnabled
      else
        console.error("[#{sJvmConsoleLogContext}] #{msg}",var1) if bJvmConsoleEnabled
    else
      console.error("[#{sJvmConsoleLogContext}] #{msg}") if bJvmConsoleEnabled


  warn: (msg,var1=null,var2=null,var3=null) ->
    var1 = JSON.stringify(var1) if var1 != null
    var2 = JSON.stringify(var2) if var2 != null
    var3 = JSON.stringify(var3) if var3 != null

    if var1 != null
      if var2 != null
        if var3 != null
          console.warn("[#{sJvmConsoleLogContext}] #{msg}",var1,var2,var3) if bJvmConsoleEnabled
        else
          console.warn("[#{sJvmConsoleLogContext}] #{msg}",var1,var2) if bJvmConsoleEnabled
      else
        console.warn("[#{sJvmConsoleLogContext}] #{msg}",var1) if bJvmConsoleEnabled
    else
      console.warn("[#{sJvmConsoleLogContext}] #{msg}") if bJvmConsoleEnabled

  # + 2013.2.18 tuiteraz
  dir: (msg) ->
    console.dir(msg) if bJvmConsoleEnabled


  # + 2013.2.18 tuiteraz
  group: (sTitle) ->
    s1 = "[#{sJvmConsoleLogContext}] #{JSON.stringify(sTitle)}"
    console.groupCollapsed(s1) if bJvmConsoleEnabled


  # + 2013.2.18 tuiteraz
  group_end: ->
    console.groupEnd() if bJvmConsoleEnabled








