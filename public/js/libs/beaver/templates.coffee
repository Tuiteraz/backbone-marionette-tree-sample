define [
  'jquery'
  'beaver-console'
], (mdlJQuery,jconsole) ->

  #+2014.1.20 tuiteraz
  twbp_alert: (sMessage,sTitle="Warning!",sType="warning",bDismissable=true)->
    if bDismissable
      sClassDismissable = "alert-dismissable"
      sCloseBtn = @button "close","", "data-dismiss='alert' aria-hidden='true'", [ "&times;" ]
    else
      sClassDismissable = ""
      sCloseBtn = ""

    @div "alert alert-#{sType} #{sClassDismissable}", [
      sCloseBtn
      @span "","","style='font-weight:bold;'", sTitle+" "
      sMessage
    ]

  # +2013.2.6 tuiteraz
  twbp_caret: ->
    """<b class="caret"/>"""

  # *2012.12.21 tuiteraz
  # *2013.5.19 tuiteraz
  fa_icon: (sName,sExtraClass='') ->
    sName = sName.replace /(remove|close)/i,'times'
    sHtml = "<i class='fa fa-#{sName} #{sExtraClass}'></i>"
    return sHtml

  # *2012.12.21 tuiteraz
  # *2013.5.19 tuiteraz
  twbp_icon: (sName) ->
    sName = 'remove' if sName == 'close'

    sHtml = "<i class='icon-#{sName}'></i>"

  # type - 'success', 'error', 'alert'
  # *2012.12.21 tuiteraz
  # *2013.1.10 tuiteraz: processing sMsg as array in case of mongo errors
  # *2013.1.21 tuiteraz
  twbp_message: (sType,sMsg,sTitle=null, sSizeClass='span4') ->
    if (sType == 'success') or (sType == 'error') or (sType == 'info')
      sAlertClass = "alert-#{sType}"
    else
      sAlertClass = ''

    if _.isNull sTitle
      sTitleHtml = ''
    else
      sTitleHtml = """<span class="label label-warning">#{sTitle}</span> """

    if _.isArray sMsg
      sMsgHtml = "<ul>"
      for sTxt in sMsg
        sMsgHtml += "<li>#{sTxt}</li>"
      sMsgHtml += "</ul>"
    else
      sMsgHtml = sMsg

    sHtml ="""
      <div class="overlayed-msg #{sSizeClass}" style="display:none;">
        <div class="msg-shadow alert #{sAlertClass} fade in">
          <a class="close" data-dismiss="alert">&#215;</a>
          <div id="flash_notice" >
           #{sTitleHtml}
           #{sMsgHtml}
          </div>
        </div>
      </div>
    """

  # *2012.12.21 tuiteraz
  twbp_required: ->
    sHtml = """
          <a href='' rel="tooltip"
             class='required-field-marker'
             data-original-title="Обязательно для заполнения" tabindex=-1>
             <span>*</span></a>
        """
  # +2013.10.28 Tuiteraz
  article: (sClass, sId='', sParams='', aInnerTxt='') ->
    @html_tag 'article',sClass,sId,sParams,aInnerTxt

  # +2013.10.31 Tuiteraz
  button: (sClass, sId='', sParams='', aInnerTxt='') ->
    @html_tag 'button',sClass,sId,sParams+" type='button'",aInnerTxt

  # +2013.11.14 Tuiteraz
  clearfix: () ->
    @html_tag 'div',"clearfix","","",[]

  #+2014.2.24 tuiteraz
  twbp_checkbox_fa:(hElement, bValue=false)->
    if bValue
      sChecked = "checked"
      sIcon    = "check-square-o"
    else
      sChecked = ""
      sIcon    = "square-o"

    if defined hElement.sIdClass
      sIdClass = hElement.sIdClass
    else
      sIdClass = ''

    @div "form-group","",[
      @input "checkbox-fa-input #{hElement.sIdClass}",
        hElement.id,
        "type='checkbox' #{sChecked} style='display:none;' data-id-class=#{sIdClass}"," "
      @label "control-label #{hElement.sLabelClass}","","for='#{hElement.id}'",[
        hElement.sLabel
      ]
      @div "col-md-1 control-label",[
        @fa_icon("#{sIcon} fa-fw")
      ]
    ]


  # element is a hash (help: Документация\параметры элементов формы)
  # *2012.11.15 tuiteraz - tooltip,checkbox support
  # *2012.11.19 tuiteraz: cg_class - безусловно добавляемый класс для div.control-group
  # *2012.12.21 tuiteraz
  # *2013.1.10 tuiteraz: +sHelp
  # *2013.1.15 tuiteraz: +sLabelStyle
  # *2013.1.23 tuiteraz: tabindex=1 for readonly
  # *2013.1.29 tuiteraz: вывод тултипа по полю sTooltip | sDescription
  # *2013.2.20 Tuiteraz: +sCGid
  # *2013.3.1 Tuiteraz: +hBindedElement -  для выбора типа ссылки. Выводить в ту же CG
  # *2013.3.1 Tuiteraz: +bNoCG - все тоже самое но выводить без CG
  # *2013.3.7 Tuiteraz: +data-placeholder for chosen.js select
  # *2013.3.22 Tuiteraz: +sIdClass - если выводится несколько форм однородных объектов то по классу +id
  #   можно идентифицировать каждый набор
  # *2013.5.7 tuiteraz: +bNoSpinner - не выводить кнокпи спиннера для цифр
  # *2013.6.1 tuiteraz: +sHelpInline - подсказка в одной строке с контролом
  # *2013.6.1 tuiteraz: +bRequiredPlaceBefore - если нет label то ставить звезду слева от контрола а не справа
  twbp_input: (hElement, sValue=null, hBindedElement=null, bNoCG=false) ->
    # для упрощения передачи параметров, когда нужно только одн доп условие передать
    if defined hBindedElement
      if _.isBoolean hBindedElement
        bNoCG = hBindedElement
        hBindedElement = null

    if !_.isNull(sValue)
      sValueProp = " value='#{sValue}'"
    else
      sValue     = ""
      sValueProp = ""

    if defined(hBindedElement) and _.isObject(hBindedElement)
      if defined hBindedElement.sCGid
        sBindedCGid = hBindedElement.sCGid
      else
        sBindedCGid = ''
      sBindedElementHtml = @div 'm-top10',sBindedCGid,"style='display:none;'", [ @twbp_input(hBindedElement, true) ]
    else
      sBindedElementHtml =''

    if !defined hElement
      return

    if defined hElement.sBindedToId
      sBindedToId = "data-binded-to-id='#{hElement.sBindedToId}'"
    else
      sBindedToId = ""

    if defined hElement.bNoSpinner
      bNoSpinner = hElement.bNoSpinner
    else
      bNoSpinner = false

    if defined hElement.sIdClass
      sIdClass = hElement.sIdClass
    else
      sIdClass = ''

    if defined hElement.bRequiredPlaceBefore
      bRequiredPlaceBefore = hElement.bRequiredPlaceBefore
    else
      bRequiredPlaceBefore = false


    if defined(hElement.sHelp) or defined(hElement.sDescription)
      sHelp = hElement.sHelp or hElement.sDescription
      sHelpHtml = """ <span class="help-block" style="">#{sHelp}</span>"""
    else if defined(hElement.sHelpInline)
      sHelp = hElement.sHelpInline
      sHelpHtml = """ <span class="help-inline">#{sHelp}</span>"""
    else
      sHelpHtml = ''


    if defined hElement.sCGid
      sCGid = hElement.sCGid
    else
      sCGid = ''

    if hElement.sInputStyle == FRM.input.style.select
      if defined hElement.sPlaceholder
        sPlaceholder = """ data-placeholder="#{hElement.sPlaceholder}" """
        sPlaceholderTxt = hElement.sPlaceholder
      else
        sPlaceholder = """ data-placeholder="..." """
        sPlaceholderTxt = ""

      hElement.bUseEmptyOption = true if _.isUndefined(hElement.bUseEmptyOption)
    else
      if defined hElement.sPlaceholder
        sPlaceholder = """ placeholder="#{hElement.sPlaceholder}" """
      else
        sPlaceholder = ''

    if defined(hElement.sTooltip)
      sTooltip = hElement.sTooltip
      sTooltipHtml = """ rel="tooltip" data-original-title="#{sTooltip}" """
    else
      sTooltipHtml = ''

    if defined hElement.bReadOnly
      if hElement.bReadOnly
        sReadOnlyClass    = "uneditable-#{hElement.sInputStyle}"
        sReadOnlyBtnClass = "disabled"
        sReadOnlyAttr     = 'readonly="readonly" tabindex="-1"'
      else
        sReadOnlyClass    = ''
        sReadOnlyBtnClass = ''
        sReadOnlyAttr     = ''
    else
      sReadOnlyClass    = ''
      sReadOnlyBtnClass = ''
      sReadOnlyAttr     = ''

    if defined hElement.sLabelClass
      sLabelClass = """ #{hElement.sLabelClass} """
    else
      sLabelClass = ''

    if defined hElement.sLabelStyle
      sLabelStyle = """ style="#{hElement.sLabelStyle}" """
    else
      sLabelStyle = ''

    sStar = if hElement.bRequired then @twbp_required()  else ""

    if defined hElement.sLabel
      sLabel = """<label class="control-label #{sLabelClass}" for="#{hElement.id}" #{sLabelStyle}>#{hElement.sLabel}</label>"""
      sControlsClass=''
      if bRequiredPlaceBefore
        sInputBefore = sStar
        sInputAfter = ""
      else
        sInputBefore = ""
        sInputAfter = sStar
    else
      sLabel = ''
      sControlsClass=''
      if bRequiredPlaceBefore
        sInputBefore = sStar
        sInputAfter = ""
      else
        sInputBefore = ""
        sInputAfter = sStar

    if defined hElement.sSizeClass
      sSizeClass = "#{hElement.sSizeClass}"
    else
      sSizeClass = ''

    sDefInpClass = "form-control"

    if hElement.sInputStyle == FRM.input.style.radio_v
      if defined hElement.sClass
        sClass = """ class="#{hElement.sClass} #{sReadOnlyClass} no-margin-top #{sIdClass} #{sDefInpClass}" """
      else
        sClass = """ class="#{sReadOnlyClass} no-margin-top #{sIdClass} #{sDefInpClass}" """
    else if hElement.sInputStyle == FRM.input.style.radio_h
      if defined hElement.sClass
        sClass = """ class="#{hElement.sClass} #{sReadOnlyClass} no-margin-top #{sIdClass} #{sDefInpClass}" """
      else
        sClass = """ class="#{sReadOnlyClass} no-margin-top #{sIdClass} #{sDefInpClass}" """
    else if hElement.sInputStyle == FRM.input.style.checkbox
      if defined hElement.sClass
        sClass = "#{hElement.sClass} #{sReadOnlyClass} #{sIdClass} #{sDefInpClass}"
      else
        sClass = "#{sReadOnlyClass} #{sIdClass} #{sDefInpClass}"

    else
      if defined hElement.sClass
        sClass = """ class="#{hElement.sClass} #{sReadOnlyClass} #{sIdClass} #{sDefInpClass}" """
      else
        sClass = """ class="#{sReadOnlyClass} #{sIdClass} #{sDefInpClass}" """


    if defined hElement.sAttrs
      sAttrs = "#{hElement.sAttrs} #{sReadOnlyAttr}"
    else
      sAttrs = "#{sReadOnlyAttr}"

    if defined hElement.sInputType
      sType = """ type="#{hElement.sInputType}" """
    else
      sType = ''

    if defined hElement.sCgClass
      sCgClass = "#{hElement.sCgClass}"
    else
      sCgClass = ''


    sId = if defined hElement.id then hElement.id else ''

    # отсюда начинаем формировать html --------------------------------
    if hElement.sInputType == FRM.input.type.number
      if !bNoSpinner
        aSpinnerBtnsHtml = [
          @html_tag 'button', "btn btn-mini spinner-btn-up #{sReadOnlyBtnClass}",'',
            "data-target-input-id='#{sId}' tabindex=-1",@fa_icon('chevron-up')
          @html_tag 'button', "btn btn-mini spinner-btn-down #{sReadOnlyBtnClass}",'',
            "data-target-input-id='#{sId}' tabindex=-1",@fa_icon('chevron-down')
        ]
      else
        aSpinnerBtnsHtml = []
      sTag = """
                  <#{hElement.sInputStyle}
                    #{sType}
                    id="#{sId}"
                    #{sPlaceholder}
                    #{sClass}
                    #{sAttrs}
                    #{sTooltipHtml}
                    #{sValueProp}
                    data-id-class=#{sIdClass}
                    />

            """
      sTag += @div('',sCGid,"style='width:20px;height:20px;display:inline-block;'", aSpinnerBtnsHtml) if !_.isEmpty(aSpinnerBtnsHtml)
      sTag += @div 'clearfix'
      sTag += sHelpHtml
      if !bNoCG
        sRes =  @div "form-group #{sCgClass}", [
                  sLabel
                  sInputBefore
                  @div "#{sControlsClass} #{sSizeClass}", '', "", sTag
                  sInputAfter
                  sBindedElementHtml
                ]
      else
        sRes = sLabel + sInputBefore +
               @div("#{sControlsClass} #{sSizeClass}", '', "", sTag) +
               sInputAfter +
               sBindedElementHtml # это если вдруг будет второгоу ровня вложение элементов

    else if hElement.sInputStyle == FRM.input.style.select
      sTag = """
        <#{hElement.sInputStyle}
          #{sType} id="#{sId}"
          #{sPlaceholder}
          #{sClass}
          #{sAttrs}
          #{sBindedToId}
          #{sTooltipHtml}
          data-id-class=#{sIdClass}
        >
      """
      if defined hElement.hOptions
        if hElement.bUseEmptyOption
          sTag += """<option value="">#{sPlaceholderTxt}</option>"""
        iIdx = 0
        for sOptValue,sOptLabel of hElement.hOptions
          if !_.isUndefined(hElement.iDefaultOptionIdx)
            if iIdx == hElement.iDefaultOptionIdx
              sSelected = "selected"
            else
              sSelected = ""
          else if !_.isEmpty(sValue)
            if sValue == sOptValue
              sSelected = "selected"
            else
              sSelected = ""
          else
            sSelected = ""

          sTag += """<option value="#{sOptValue}" #{sSelected}>#{sOptLabel}</option>"""
          iIdx += 1

      sTag += "</select>"

      if !bNoCG
        sRes = @div "form-group #{sCgClass}", sCGid, [
          sLabel
          sInputBefore
          @div "#{sControlsClass} #{sSizeClass}", sTag
          sInputAfter
          sHelpHtml
          sBindedElementHtml
        ]
      else
        sRes = sLabel + sInputBefore +
               @div("#{sControlsClass} #{sSizeClass}", sTag) +
               sInputAfter + sHelpHtml +
               sBindedElementHtml

    else if hElement.sInputStyle == FRM.input.style.radio_v
      sTag = ''
      iElemCount = 1
      if _.isNull(hElement.id) and !_.isNull(hElement.sIdClass)
        sName = hElement.sIdClass
      else
        sName = hElement.id

      for sOptCode,sOptLabel of hElement.hOptions
        sAddAttrs = if iElemCount == 1 then "checked='checked'"
        sTag += """<input type="radio"
                                  name="#{sName}"
                                  data-opt-code="#{sOptCode}"
                                  #{sClass} #{sAttrs} #{sAddAttrs} data-id-class=#{sIdClass}>
                            #{sOptLabel} </input><br>"""
        iElemCount += 1

      if !bNoCG
        sRes =  @div "form-group #{sCgClass}", sCGid, [
          sLabel
          @div "well white-back #{sSizeClass} no-margin-left p-all10 no-margin-bottom",sTag
          @div "clearfix"
          sBindedElementHtml
        ]
      else
        sRes = sLabel +
               @div("well white-back #{sSizeClass} no-margin-left p-all10 no-margin-bottom",sTag) +
               @clearfix() +
               sBindedElementHtml

    else if hElement.sInputStyle == FRM.input.style.radio_h
      sTag = ''
      iElemCount = 1
      if _.isNull(hElement.id) and !_.isNull(hElement.sIdClass)
        sName = hElement.sIdClass
      else
        sName = hElement.id

      for sOptCode,sLabel of hElement.hOptions
        sAddAttrs = if iElemCount == 1 then "checked='checked'" else ''
        sSpacer = if iElemCount == 1 then """ <div style="width:10px;display:inline-block;"></div> """ else ''
        sTag += """
                <input type="radio"
                name="#{sName}"
                data-opt-code="#{sOptCode}"
                #{sClass} #{sAttrs} #{sAddAttrs} data-id-class=#{sIdClass}>
                #{sLabel}</input>#{sSpacer}
                """
        iElemCount += 1

      if !bNoCG
        sRes = @div "form-group #{sCgClass}", sCGid, [
          @div "well no-border no-back pull-left no-margin-left p-all10 no-shadow no-padding-left", sLabel
          @div "well white-back #{sSizeClass} no-margin-left p-all10 no-margin-bottom", sTag
          @clearfix()
        ]
      else
        sRes = @div("well no-border no-back pull-left no-margin-left p-all10 no-shadow no-padding-left", sLabel) +
               @div("well white-back #{sSizeClass} no-margin-left p-all10 no-margin-bottom", sTag) +
               @clearfix() +
               sBindedElementHtml

    else if hElement.sInputStyle == FRM.input.style.checkbox
      sDisabled = if hElement.bReadOnly then "disabled='disabled'" else ''
      sTag = @html_tag 'label','checkbox',
      @html_tag 'input',sClass, sId, "#{sAttrs} #{sDisabled} type='checkbox' #{sTooltipHtml} ", hElement.sLabel

      if !bNoCG
        sRes = @div "form-group #{sCgClass}", sCGid, [
          sInputBefore
          @div sSizeClass, sTag
          sInputAfter
          sBindedElementHtml
        ]
      else
        sRes = sInputBefore +  @div(sSizeClass, sTag) + sInputAfter +
               sBindedElementHtml

    else if hElement.sInputStyle == FRM.input.style.text
      sCgClass += if hElement.sInputType ==' hidden' then ' no-margin' else ''
      sTag = """
                  <#{hElement.sInputStyle} #{sType}
                      id="#{sId}"
                      #{sPlaceholder}
                      #{sClass}
                      #{sAttrs}
                      data-id-class=#{sIdClass}
                  >#{sValue}</#{hElement.sInputStyle}>
                   """

      if !bNoCG
        sRes = @div "form-group #{sCgClass}", sCGid, [
          sLabel
          sInputBefore
          @div "#{sControlsClass} #{sSizeClass}", '', [
            sTag
            sHelpHtml
          ]
          sInputAfter
        ]
      else
        sRes = sLabel + sInputBefore +
        @div("#{sControlsClass} #{sSizeClass}", '', [sTag,sHelpHtml])+
        sInputAfter +
        sBindedElementHtml

    else
      sCgClass += if hElement.sInputType ==' hidden' then ' no-margin' else ''
      sTag = """
                  <#{hElement.sInputStyle} #{sType}
                      id="#{sId}"
                      #{sPlaceholder}
                      #{sClass}
                      #{sAttrs}
                      #{sValueProp}
                      data-id-class=#{sIdClass}
                  />

                   """

      if !bNoCG
        sRes = @div "form-group #{sCgClass}", sCGid, [
          sLabel
          sInputBefore
          @div "#{sControlsClass} #{sSizeClass}", '', [
            sTag
            sHelpHtml
          ]
          sInputAfter
        ]
      else
        sRes = sLabel + sInputBefore +
               @div("#{sControlsClass} #{sSizeClass}", '', [sTag,sHelpHtml])+
               sInputAfter +
               sBindedElementHtml

    sRes

  # +2012.11.13 Tuiteraz
  # *2012.12.21 Tuiteraz
  a: (sHref='', sParams='', sInnerTxt='', sClass='') ->
    if defined sInnerTxt
      sParams += " href='#{sHref}' "
    else if !defined(sInnerTxt) && defined(sParams)
      sInnerTxt = sParams
      sParams = " href='#{sHref}' "
    @html_tag 'a',sClass,'',sParams,sInnerTxt

  # хелпер для кнопки с иконкой и текстом
  # sIcon - имя иконки без приставки "icon-"
  # sActionId - код действия
  # sTooltip - подсказка
  # sInnerTxt - текст который будет стоять рядом с иконкой
  # sClass - дополнительный класс кнопки
  # sIconAlign - ориентация иконки: left | right. Имеет смысл только при наличии sInnerTxt
  # +2012.11.14 Tuiteraz
  # *2012.12.21 Tuiteraz
  # *2013.2.16 Tuiteraz: sParams
  # *2013.4.6 Tuiteraz: sTag
  # *2013.4.26 Tuiteraz: если не указана иконка то не выводить её тэг
  # *2014.1.20 Tuiteraz: if sIcon is Object get props from i t
  icon_button: (sIcon='', sAction='', sTooltip='', sInnerText='', sClass='',sIconAlign='left', sParams='', sTag='a') ->
    if _.isObject(sIcon)
      hParams = sIcon
      sIcon = ""
    else
      hParams= {}

    hParams.sIcon      ||= sIcon
    hParams.sAction    ||= sAction
    hParams.sActionAttrName ||= "data-action"
    hParams.sTooltip   ||= sTooltip
    hParams.sInnerText ||= sInnerText
    hParams.sClass     ||= sClass
    hParams.sIconAlign ||= sIconAlign
    hParams.sParams    ||= sParams
    hParams.sTag       ||= sTag

    if _.isEmpty(hParams.sClass)
      sResClass = ""
    else
      sResClass = "btn #{hParams.sClass}"

    sParams += " type='button' #{hParams.sActionAttrName}='#{hParams.sAction}' rel='tooltip' data-original-title='#{hParams.sTooltip}' #{hParams.sParams}"
    sIcon = if defined hParams.sIcon then @fa_icon(hParams.sIcon) else ''
    if defined hParams.sInnerText
      if hParams.sIconAlign == 'left'
        sInnerText = sIcon + "&nbsp;" + hParams.sInnerText
      else if hParams.sIconAlign == 'right'
        sInnerText +=  "&nbsp;" + sIcon
    else
      sInnerText = sIcon

    @html_tag hParams.sTag,sResClass,'',sParams,sInnerText

  # sSrc - строка без 'assets/', например "root.jpg"
  # +2013.2.4 Tuiteraz
  # *2013.6.3 Tuiteraz: +sParams
  # *2013.7.29 Tuiteraz: - assets
  img: (sSrc,sClass='', sParams='') ->
    sParams += " src='#{sSrc}'"
    @html_tag 'img',sClass,'',sParams,''

  #+2014.1.23 tuiteraz
  lazy_img: (sImgSrc,sLoaderSrc,sClass="")->
    @img(sLoaderSrc,"lazy #{sClass}","data-src='#{sImgSrc}'")

  # +2013.4.16 tuiteraz
  svg: (sSVGSrc,sIEImgSrc,sClass='') ->
    """
    <!--[if !IE]>-->
      #{ @img(sSVGSrc, sClass) }
    <!--<![endif]-->
    <!--[if IE]>
      #{ @img(sIEImgSrc, sClass) }
    <![endif]-->
    """

  # +2013.12.12 Tuiteraz
  textarea: (sClass, sId='', sParams='', aInnerTxt='') ->
    @html_tag 'textarea',sClass,sId,sParams,aInnerTxt


  # +2014.2.5 Tuiteraz
  label: (sClass, sId='', sParams='', aInnerTxt='') ->
    @html_tag 'label',sClass,sId,sParams,aInnerTxt

  # +2012.11.13 Tuiteraz
  div: (sClass, sId='', sParams='', aInnerTxt='') ->
    @html_tag 'div',sClass,sId,sParams,aInnerTxt

  # +2014.1.15 Tuiteraz
  dl: (sClass, sId='', sParams='', aInnerTxt='') ->
    @html_tag 'dl',sClass,sId,sParams,aInnerTxt

  # +2014.1.15 Tuiteraz
  dt_dd: (sDtText,sDdText) ->
    sHtml = @html_tag 'dt',"","","",[sDtText]
    sHtml += @html_tag 'dd',"","","",[sDdText]
    return sHtml

  # checkbox via FontAwesome
  # подразумевается что FontAwesome уже подключен в страницу
  # label используется для замещения checkbox
  # в стилях должны быть следующие установки:
  #  input[type=checkbox] {
  #    display:none;
  #    & + label:before{
  #      font-family: "FontAwesome";
  #      content: "\f096";
  #      height: $font-height;
  #      width: $font-height;
  #      line-height: $line-height;
  #      display:inline-block;
  #      padding: 0 0 0 0px;
  #    }
  #    &:checked + label:before{
  #      font-family: "FontAwesome";
  #      content: "\f046";
  #      height: $font-height;
  #      width: $font-height;
  #      line-height: $line-height;
  #      display:inline-block;
  #      padding: 0 0 0 0px;
  #    }
  #  }
  # +2013.5.28 tuiteraz
  # *2013.9.24 tuiteraz: i inside label fo ie8
  checkbox_fa: (sClass, sId='', sParams='', sInnerTxt='',sAddIconClass='pull-right') ->
    @div sClass,"", [
      @input('checkbox-fa-input',sId,sParams+" type='checkbox'"," ") +
      """
      <label for="#{sId}" class="unselectable">
      <i class="fa fa-square-o #{sAddIconClass}"/>
      <span class="checkbox-fa-input-label-text">#{sInnerTxt}</span>
      <div class='clearfix' />
      </label>
      """
      @clearfix()
    ]


  # +2013.4.21 Tuiteraz
  input: (sClass, sId='', sParams='', aInnerTxt='') ->
    @html_tag 'input',sClass,sId,sParams,aInnerTxt

  # +2013.4.14 Tuiteraz
  nav: (sClass, sId='', sParams='', aInnerTxt='') ->
    @html_tag 'nav',sClass,sId,sParams+" role='navigation'",aInnerTxt

  # +2013.4.14 Tuiteraz
  section: (sClass, sId='', sParams='', aInnerTxt='') ->
    @html_tag 'section',sClass,sId,sParams,aInnerTxt

  # +2012.11.13 Tuiteraz
  # *2013.4.7 Tuiteraz: +sParams
  h:(iSize, sInnerTxt,sClass=null,sParams=null) ->
    @html_tag "h#{iSize}",sClass,'',sParams,sInnerTxt

  # +2013.2.4 Tuiteraz
  header:(sInnerTxt,sClass=null) ->
    @html_tag "header",sClass,'','',sInnerTxt

  # +2012.11.13 Tuiteraz
  form: (sClass='', sId='', sParams='', aInnerTxt) ->
    @html_tag 'form',sClass,sId,sParams+" role='form'",aInnerTxt

  # +2012.11.13 Tuiteraz
  ul: (sClass, sId='', sParams='', aInnerTxt='') ->
    @html_tag 'ul',sClass,sId,sParams,aInnerTxt

  # +2012.11.13 Tuiteraz
  form: (sClass='', sId='', sParams='', aInnerTxt) ->
    @html_tag 'form',sClass,sId,sParams+" role='form'",aInnerTxt

  # +2013.3.16 Tuiteraz
  legend: (sInnerTxt='', sClass='', sParams='') ->
    @html_tag 'legend',sClass,"",sParams,sInnerTxt


  # +2012.11.13 Tuiteraz
  li: (sClass, sId='', sParams='', aInnerTxt='') ->
    @html_tag 'li',sClass,sId,sParams,aInnerTxt

  # +2014.1.23 Tuiteraz
  p: (aInnerTxt='',sClass='', sId='', sParams='') ->
    @html_tag 'p',sClass,sId,sParams,aInnerTxt

  # +2012.11.22 Tuiteraz
  span: (sClass, sId='', sParams='', aInnerTxt='') ->
    @html_tag 'span',sClass,sId,sParams,aInnerTxt

  # +2012.11.22 Tuiteraz
  table: (sClass, sId='', sParams='', aInnerTxt='') ->
    @html_tag 'table',sClass,sId,sParams,aInnerTxt

  # +2012.11.22 Tuiteraz
  thead: (sClass, sId='', sParams='', aInnerTxt='') ->
    @html_tag 'thead',sClass,sId,sParams,aInnerTxt

  # +2012.11.22 Tuiteraz
  tbody: (sClass, sId='', sParams='', aInnerTxt='') ->
    @html_tag 'tbody',sClass,sId,sParams,aInnerTxt

  # +2012.11.22 Tuiteraz
  td: (sClass, sId='', sParams='', aInnerTxt='') ->
    @html_tag 'td',sClass,sId,sParams,aInnerTxt

  # +2012.11.22 Tuiteraz
  tr: (sClass, sId='', sParams='', aInnerTxt='') ->
    @html_tag 'tr',sClass,sId,sParams,aInnerTxt

  # +2012.11.22 Tuiteraz
  tab_btn_close: (sTabId) ->
    @html_tag "button", "pull-right close btn-close-link", "",
      "data-tab-can-close='true' data-tab-id='#{sTabId}'", "&times;"


  # отрисовываем html таблицы списка данных
  # sTabId - id объекта закладки
  # hDataAttrs - хэш атрибутов объекта передаваемый сервером
  # aData - массив данных по атрибутам
  # aAttrsOrder - колонки атрибутов в том порядке, в котором должны выыводиться в таблице
  #               при этом указывается их код указанный в поле "code" confman
  # +2012.11.21 Tuiteraz
  # *2012.12.21 Tuiteraz
  # *2013.2.25 Tuiteraz: table align center
  # *2013.2.26 Tuiteraz: подстановка значения из списка hFormElements вместо кода значения из сервера
  # +2013.3.24 tuiteraz: вывод заголовка таблицы - название колонки береться из кода формы
  data_table: (sTabId, hDataAttrs, aData, hElements, aAttrsOrder ) ->

    sCols = ''
    for sCode in aAttrsOrder
      sCols += @html_tag "th", [ hElements[sCode].sLabel]

    sRows = ''
    for hItem in aData
      # h_item=
      #   id
      #   h_attributes

      sRowCols = ''
      for sAttrCode in aAttrsOrder
        if defined hItem.hAttributes
          if defined hItem.hAttributes[sAttrCode] # если значение атрибута есть
              if defined hElements[sAttrCode] # если указаны устаноки отображения атрибута
                if hElements[sAttrCode].sInputStyle != 'select'
                  sData = j.object.attr.value hItem.hAttributes[sAttrCode], hElements[sAttrCode],false
                else sData = hElements[sAttrCode].hOptions[hItem.hAttributes[sAttrCode]]
              else sData = j.object.attr.value hItem.hAttributes[sAttrCode], hElements[sAttrCode], false
          else sData = '<>'
        else sData = '<>'
        sClass = if defined hElements[sAttrCode].sClass then hElements[sAttrCode].sClass else ''
        sAttrs = if defined hElements[sAttrCode].sAttrs then hElements[sAttrCode].sAttrs else ''
        sRowCols += @html_tag 'td',sClass,'', sAttrs, [ sData ]

      sRows += @html_tag 'tr','',"#{hItem.id}", [sRowCols]

    sClass = "table table-striped table-bordered table-condensed table-hover"
    @html_tag 'table',sClass,"table-#{sTabId}","align='center'", [
      @html_tag 'thead', [ @html_tag 'tr',[sCols] ]
      @html_tag 'tbody', [ sRows ]
    ]

  # Шаблон для упрощенного вывода закладок
  # sClass - если это строка, то применяется только для корневого div элемента .tabbable
  # если массив, тогда [0] - для корня, [1] - для ul, [2] - для div.tab-content
  # +2012.11.13 Tuiteraz
  tabbable: (sClass, sId='', sParams='', aLiInnerText='', aTabs='') ->
    jconsole.enable_log 'tabbable()'

    if typeof sClass == 'string'
      sTabbableClass = sClass
      sNavClass      = ''
      sContentClass  = ''
    else if typeof sClass == 'object'
      sTabbableClass = sClass[0]
      sNavClass      = if defined sClass[1] then sClass[1] else ''
      sContentClass  = if defined sClass[2] then sClass[2] else ''



    # попустить a_li_text или a_tabs нельзя, они всегда идут друг за другом
    if typeof(sId) == 'object' #
      aLiInnerText = sId
      aTabs    = sParams
      sId      = ''
      sParams  = ''
    else if typeof(sParams) == 'object'
      aTabs    = aLiInnerText
      aLiInnerText = sParams
      sParams  = ''

    sNavClass = if !defined sNavClass then 'nav-tabs' else sNavClass

    #jconsole.log "sClass=%s",sClass
    #jconsole.log "sId=%s",sId
    #jconsole.log "sParams=%s",sParams
    #jconsole.log "a_li_inner_text=%s",a_li_inner_text
    #jconsole.log "a_tabs=%s",a_tabs

    aLi = []
    for iIdx,item of aLiInnerText
      sClass = if parseInt(iIdx)==0 then "class='active'" else ''
      #jconsole.log "i_idx=#{i_idx}, sClass='#{sClass}'"
      aLi.push "<li #{sClass}>#{item}</li>"

    sHtml = @div "tabbable #{sTabbableClass}", [
      @ul "nav #{sNavClass}", aLi
      @div "tab-content #{sContentClass}", aTabs
    ]


  # Универсальная процка вывода хтмл тэга
  # вместо sId можно подставлять функцию наполнения, если нет айди и параметров
  # вместо sParams можно подставлять функцию наполнения, если нет параметров
  # +2012.11.13 Tuiteraz
  # *2012.11.15 Tuiteraz - распознавать a_inner_text из строки
  # *2013.2.4 Tuiteraz: +img
  # *2013.4.21 Tuiteraz: +input
  html_tag: (sTag, sClass='', sId='', sParams='', aInnerTxt='') ->
    if typeof(sClass) == 'object'
      aInnerTxt = sClass
      sClass = ''
    else if typeof(sId) == 'object'
      aInnerTxt = sId
      sId = ''
    else if typeof(sParams) == 'object'
      aInnerTxt = sParams
      sParams = ''
    else if (typeof(sId) == 'string')&&(!defined sParams)&&(!defined aInnerTxt)
      aInnerTxt = [sId]
      sId = ''
    else if (typeof(sParams) == 'string')&&(!defined aInnerTxt)&&(sTag != 'img')
      aInnerTxt = [sParams]
      sParams = ''
    else if (typeof(aInnerTxt) == 'string')
      aInnerTxt = [aInnerTxt]

    #jconsole.log "(2)sTag=#{sTag}, sClass=#{sClass}, sId=#{sId}, sParams=#{sParams}, aInnerTxt=#{aInnerTxt}"

    sHtml = ''
    sHtml += item for item in aInnerTxt
    sTagClass = if defined(sClass) then "class='#{sClass}'" else ''
    sTagId = if defined(sId) then "id='#{sId}'" else ''
    sTagParams = if defined(sParams) then sParams else ''

    if sTag == 'img'
      html = """<#{sTag} #{sTagClass} #{sTagParams}/>"""
    else if sTag == 'input'
      html = """<#{sTag} #{sTagClass} #{sTagId} #{sTagParams}>"""
    else
      html = """<#{sTag} #{sTagClass} #{sTagId} #{sTagParams}>#{sHtml}</#{sTag}>"""





