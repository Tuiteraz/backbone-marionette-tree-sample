(function addXhrProgressEvent($) {
    var originalXhr = $.ajaxSettings.xhr;
    $.ajaxSetup({
        xhr: function() {
            var req = originalXhr(), that = this;
            if (req) {
                if (typeof req.addEventListener == "function" && that.progress !== undefined) {
                    req.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            iPercentComplete = parseInt( (evt.loaded / evt.total * 100), 10)
                        }else{
                            iPercentComplete = 0
                        }
                        that.progress(evt,iPercentComplete);
                    }, false);
                }
                if (typeof req.upload == "object" && that.progressUpload !== undefined) {
                    req.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            iPercentComplete = parseInt( (evt.loaded / evt.total * 100), 10)
                        }else{
                            iPercentComplete = 0
                        }

                        that.progressUpload(evt,iPercentComplete);
                    }, false);
                }
            }
            return req;
        }
    });
})(jQuery);