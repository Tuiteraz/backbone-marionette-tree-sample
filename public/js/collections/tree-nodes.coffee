define [
  "beaver-console"
  'models/tree-node'
],(
  jconsole
  TreeNode
)->
  sLogHeader = "[collections/tree-nodes]"
  jconsole.group "#{sLogHeader}"

  Collection = Backbone.Collection.extend {
    model: TreeNode

#    localStorage : new Backbone.LocalStorage('hln-links')
#    initialize: ->
#      @cid = "tree-nodes"
  }

  jconsole.info "loaded"
  jconsole.group_end()

  return Collection

