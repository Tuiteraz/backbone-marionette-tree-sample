define [
  "beaver-console"
  "views/app-view"
],(
  jconsole
  AppView
)->

  initialize:()->
    sLogHeader = "[app].initialize()"
    jconsole.info "#{sLogHeader}"

    @jAppView    = new AppView()