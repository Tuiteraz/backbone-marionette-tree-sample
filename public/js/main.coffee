window.bJvmConsoleEnabled = true
window.sJvmConsoleLogContext = "APP"

if typeof requirejs == "undefined"
  jHead = document.getElementsByTagName('head')[0]
  jScript = document.createElement('script')
  jScript.setAttribute('type','text/javascript')
  jScript.setAttribute('src','js/libs/require-js/require.js')
  jScript.setAttribute('data-main','js/main.js')
  jHead.appendChild(jScript)

  return

console.groupCollapsed("loading modules & confs ...") if bJvmConsoleEnabled

requirejs.config {
#  baseUrl : '/public'
  waitSeconds: 5
  packages: [
    {
      name: "text"
      location : "libs/require-js/plugins"
      main: 'text'
    }
    {
      name: "tpl"
      location : "libs/require-js/plugins"
      main: 'tpl'
    }
    {
      name: "jade"
      location : "libs/require-js/plugins"
      main: 'jade'
    }
  ]

  paths: {
    'jquery'             : 'libs/jquery/jquery-2.1.1.min'
    'jquery-plugins'     : 'libs/jquery/jquery.plugins'

    'bootstrap'          : 'libs/bootstrap/js/bootstrap.min'
    'bootstrap-hover-dropdown' : 'libs/bootstrap/js/twitter-bootstrap-hover-dropdown'

    'domReady'           : 'libs/require-js/plugins/domReady'
    'lib-pack'           : 'libs/lib.pack'
    'require-js'         : 'libs/require-js/require'

    'beaver-console'     : 'libs/beaver/console'
    'beaver-extensions'  : 'libs/beaver/extensions'

    'underscore'         : 'libs/underscore'
    'underscore.string'  : 'libs/underscore.string.min'
    'async'              : 'libs/async'
    'moment'             : 'libs/moment.min'

    'backbone'              : 'libs/backbone/backbone'
    'backbone.localStorage' : 'libs/backbone/backbone.localStorage'
    'backbone.marionette'   : 'libs/backbone/backbone.marionette'

  }
  shim: {
    'jquery'                         : { exports: 'jQuery' }
    'jquery-plugins'                 : ['jquery']
    'libs/beaver/helpers'            : ['jquery']

    'bootstrap'                      : ['jquery']
    'bootstrap-hover-dropdown'       : ['bootstrap']

    'beaver-console'                 : ['beaver-extensions']
    'beaver-extensions'              : ['jquery','underscore']

    'lib-pack'                       : ['beaver-console']
    'underscore'                     :
      exports : '_'
    'underscore.string'              : ['underscore']

    'backbone'                       :
      deps    : ['jquery','underscore']
      exports : 'Backbone'
    'backbone.localStorage'          : ['backbone']
    'backbone.marionette'            : ['backbone']
    'app'                            : ['backbone',
                                        'backbone.localStorage',
                                        'backbone.marionette']
  }
}

requirejs [
  'lib-pack'
  'beaver-console'
  'domReady'
  'app'
], (mdlLibPack,jconsole,domReady,jApp) ->
  jconsole.group_end()
  jconsole.info "STARTING APP..."

  domReady ->
    jconsole.log "DOM ready now!!"
    $("meta[name=fragment]").remove() # because search engine wouldn't accept html snapshot with this tag

    jApp.initialize()
    window.jApp = jApp
