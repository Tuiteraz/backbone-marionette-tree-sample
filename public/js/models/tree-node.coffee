define [
  "beaver-console"
],(
  jconsole
)->

  sLogHeader = "[models/tree-node]"
  jconsole.group "#{sLogHeader}"

  Model = Backbone.Model.extend {
    initialize: ->
      nodes = @get 'nodes'
      if !_.isUndefined nodes
        TreeNodes = require("collections/tree-nodes")
        @nodes = new TreeNodes(nodes) #store node as model class extra property not as data property
        @unset 'nodes'  # unset model attr but we still have nodes as model class prop
  }

  jconsole.info "loaded"
  jconsole.group_end()

  return Model