define [
  "beaver-console"
  "views/tree-item-view"
  "jade!templates/tree-view-tpl"
],(
  jconsole
  TreeItemView
  TreeViewTpl
)->

  sLogHeader = "[views/tree-root-view]"
  jconsole.group "#{sLogHeader}"

  View = Backbone.Marionette.CollectionView.extend {
#    id : "#tree-view-data"
    tagName : 'ul'
    itemView : TreeItemView

    initialize : ()->
      @cid = 'tree-root'
  }

  jconsole.info "loaded"
  jconsole.group_end()

  return View