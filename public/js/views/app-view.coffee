define [
  "beaver-console"
  "jade!templates/app-view-tpl"
  "views/navbar-view"
  "views/tree-root-view"
  "collections/tree-nodes"
  "text!stubs/tree-data.json"

],(
  jconsole
  AppViewTpl
  NavbarView
  TreeRootView
  TreeNodes
  sTreeData
)->

  sLogHeader = "[views/app-view]"
  jconsole.group "#{sLogHeader}"

  AppView = Backbone.View.extend {
    el : 'body'
    template : AppViewTpl

    initialize : ()->
      @cid = 'app'
      @jNavbarView = new NavbarView()

      hTreeData = $.parseJSON sTreeData

      @jTreeNodes = new TreeNodes hTreeData

      @jTreeRootView   = new TreeRootView {collection : @jTreeNodes}
      @render()

    render: ()->
      jconsole.log "#{sLogHeader}.render()"

      @$el
       .empty()
       .append(@jNavbarView.$el)
       .append(@template())

      @jTreeRootView.render()

#      @.$("##{@jTreeView.id}").append(@jTreeView.$el)
      @.$("#tree-view-data").html @jTreeRootView.el

      return this
  }

  jconsole.info "loaded"
  jconsole.group_end()

  return AppView