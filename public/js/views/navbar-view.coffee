define [
  "beaver-console"
  "jade!templates/navbar-view-tpl"
],(
  jconsole
  NavbarViewTpl
)->

  sLogHeader = "[views/navbar-view]"
  jconsole.group "#{sLogHeader}"

  View = Backbone.View.extend {

    template : NavbarViewTpl

    initialize : ()->
      @cid = 'navbar'
      @render()

    render: ()->
      jconsole.log "#{sLogHeader}.render()"
      @el = @template()
      @$el = $(@el)
      return this
  }

  jconsole.info "loaded"
  jconsole.group_end()

  return View