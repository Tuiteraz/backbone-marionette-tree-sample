define [
  "beaver-console"
  "jade!templates/tree-item-view-tpl"
],(
  jconsole
  ViewTpl
)->

  sLogHeader = "[views/tree-view]"
  jconsole.group "#{sLogHeader}"

  iItemCounter = 1

  Backbone.View.prototype.close = ->
    @onClose() if @onClose
    @remove()


  View = Backbone.Marionette.CompositeView.extend {
    template : ViewTpl
    tagName  : "li"
    events:
      "click i.item-action-remove" : "onItemRemoveClick"

    initialize : ()->
      @cid = "tree-item-#{iItemCounter}"
      @collection = @model.nodes
      iItemCounter += 1

      @listenTo @model, 'destroy', @remove

    appendHtml : (jCollectionView, jItemView)->
      jCollectionView.$("ul:first").append jItemView.el

    onCompositeModelRendered: ->
#      jconsole.log @model.get "nodeName"

    onClose:()->
      if @children.length > 0
        @children.each (jView)->
          jView.close()
      @model.destroy() # -> @remove()

    onItemRemoveClick : (jEvent)->
      jconsole.log "#{sLogHeader}.onItemRemoveClick(#{@$el.text()})"
      jEvent.stopPropagation()
      @close()

    onRender:()->
      @$("ul:first").remove() if _.isUndefined @collection
  }

  jconsole.info "loaded"
  jconsole.group_end()

  return View