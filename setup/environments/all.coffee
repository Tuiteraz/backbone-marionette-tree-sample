#--( DEPENDENCIES
log            = require("helpers/winston-wrapper")(module)
config         = require('nconf')

express        = require 'express'
expressLogger  = require '../../middlewares/express-logger'

bodyParser     = require 'body-parser'
serveStatic    = require 'serve-static'

path           = require 'path'

#--) DEPENDENCIES

rootDir = process.cwd()

module.exports = ()->
  @set 'views', path.join(rootDir,"views")
  @set 'view engine', 'jade'
  @use express.favicon()
  @use expressLogger
  @use express.cookieParser()
  @use bodyParser.urlencoded({extended:true})
  @use bodyParser.json()
  @use express.session({secret:"carpe diem"})
  @use @router
  @use serveStatic( 'public' )
  @use express.errorHandler()


