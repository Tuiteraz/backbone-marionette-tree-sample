// Generated by CoffeeScript 1.7.1
(function() {
  var Path, Settings, Swig, error, get_binded_property_params, get_creator_collection_name, get_referenced_schemas, get_subdomain_of, is_auth_tool_path, is_br_auth_path, is_br_push_notification_path, is_br_user_path, is_cp_request, is_db_path, is_filename, is_tool_path, log, write_response, _;

  _ = require("underscore");

  Swig = require('swig');

  Path = require('path');

  Settings = require("../settings").settings;

  log = _.bind(console.log, console);

  get_subdomain_of = function(sHost) {
    var aMatches, sRegEx, sRes;
    sRegEx = /(?:http[s]*\:\/\/)*(.*?)\.(?=[^\/]*\..{2,5})/i;
    aMatches = sHost.match(sRegEx);
    sRes = !_.isNull(aMatches) ? aMatches[1] : "";
    return sRes;
  };

  is_filename = function(sSample) {
    var sRegEx;
    sRegEx = /^(\/?[a-zA-Z-_0-9]+){0,10}(?:\/)([\w\.\-@^\s]+\.[A-Za-z]{2,6})$/i;
    return sRegEx.test(sSample);
  };

  error = function(iError, jResponse, sText, bPlain) {
    var jTmpl, sHtml;
    if (bPlain == null) {
      bPlain = false;
    }
    if (!bPlain) {
      jTmpl = Swig.compileFile(Path.join(process.cwd(), "/app/views/error.html"));
      sHtml = jTmpl({
        sText: sText,
        iError: iError,
        sErrorTitle: Settings.aErrorTitles[iError],
        sServer: "" + Settings.sTitle + " " + Settings.sVersion
      });
      jResponse.writeHead(iError, {
        "Content-Type": "text/html"
      });
      return jResponse.end(sHtml);
    } else {
      jResponse.writeHead(iError, {
        "Content-Type": "text/plain"
      });
      return jResponse.end(sText);
    }
  };

  is_cp_request = function(jRequest) {
    return /^https?\:\/\/[\w.-]*\/cp\/(?:[\w\W]*)(?:#!)?$/i.test(jRequest.headers.referer);
  };

  is_tool_path = function(sUri) {
    var sToolName;
    sToolName = _.find(Settings.tools.aNames, function(sToolName) {
      var jRegExp;
      jRegExp = new RegExp("^\/" + sToolName + "$", "i");
      return jRegExp.test(sUri);
    });
    if (!_.isEmpty(sToolName)) {
      return true;
    } else {
      return false;
    }
  };

  is_auth_tool_path = function(sUri) {
    var sToolName;
    sToolName = _.find(Settings.tools.aNames, function(sToolName) {
      var jRegExp;
      jRegExp = new RegExp("^\/" + sToolName + "$", "i");
      return jRegExp.test(sUri);
    });
    if (sToolName === "auth") {
      return true;
    } else {
      return false;
    }
  };

  is_br_auth_path = function(sUri) {
    var sPath;
    sPath = Path.join(Settings.bleacher_report["in"].sApiPath, Settings.bleacher_report["in"].sLoginPath);
    if (sUri === sPath) {
      return true;
    } else {
      return false;
    }
  };

  is_br_user_path = function(sUri) {
    var sPath;
    sPath = Path.join(Settings.bleacher_report["in"].sApiPath, Settings.bleacher_report["in"].sUserPath);
    if (sUri === sPath) {
      return true;
    } else {
      return false;
    }
  };

  is_br_push_notification_path = function(sUri) {
    var sPath;
    sPath = Path.join(Settings.bleacher_report["in"].sApiPath, Settings.bleacher_report["in"].sPushNotificationsPath);
    if (sUri === sPath) {
      return true;
    } else {
      return false;
    }
  };

  is_db_path = function(sUri) {
    var jRegExp;
    jRegExp = new RegExp("^\/" + Settings.mongo.sUriPath, "i");
    if (jRegExp.test(sUri)) {
      return true;
    } else {
      return false;
    }
  };

  get_referenced_schemas = function(Object, aRes) {
    var Value, sKey, _i, _len;
    if (aRes == null) {
      aRes = null;
    }
    aRes || (aRes = []);
    if (_.isObject(Object)) {
      for (sKey in Object) {
        Value = Object[sKey];
        if (sKey === 'ref') {
          aRes.push(Value);
        } else if (_.isObject(Value) || _.isArray(Value)) {
          get_referenced_schemas(Value, aRes);
        }
      }
    } else if (_.isArray(Object)) {
      for (_i = 0, _len = Object.length; _i < _len; _i++) {
        Value = Object[_i];
        if (_.isObject(Value) || _.isArray(Value)) {
          get_referenced_schemas(Value, aRes);
        }
      }
    }
    return aRes;
  };

  get_creator_collection_name = function(Mongoose, sCollectionName) {
    var mSchema, sCreatorCollName;
    mSchema = Mongoose.modelSchemas[sCollectionName];
    sCreatorCollName = mSchema.tree._creator.ref;
    return sCreatorCollName;
  };

  get_binded_property_params = function(Mongoose, sCreatorCollName, sBindedCollName) {
    var Type, hBinding, hRes, mCreatorSchema, sPropertyName, _ref, _results;
    mCreatorSchema = Mongoose.modelSchemas[sCreatorCollName];
    hRes = {};
    _ref = mCreatorSchema.tree;
    _results = [];
    for (sPropertyName in _ref) {
      Type = _ref[sPropertyName];
      if (_.isArray(Type)) {
        _results.push((function() {
          var _i, _len, _results1;
          _results1 = [];
          for (_i = 0, _len = Type.length; _i < _len; _i++) {
            hBinding = Type[_i];
            if (hBinding.ref === sBindedCollName) {
              _results1.push(hRes = {
                sPropertyName: sPropertyName,
                bIsArray: true
              });
            } else {
              _results1.push(void 0);
            }
          }
          return _results1;
        })());
      } else {
        _results.push(void 0);
      }
    }
    return _results;
  };

  write_response = function(iStatus, Res, jResponse, bNeedStringify, sContentType) {
    if (bNeedStringify == null) {
      bNeedStringify = true;
    }
    if (sContentType == null) {
      sContentType = "text/json";
    }
    jResponse.writeHead(iStatus, {
      "Content-Type": sContentType
    });
    if (bNeedStringify) {
      return jResponse.end(JSON.stringify(Res));
    } else {
      return jResponse.end(Res);
    }
  };

  exports.get_subdomain_of = get_subdomain_of;

  exports.is_cp_request = is_cp_request;

  exports.is_filename = is_filename;

  exports.is_tool_path = is_tool_path;

  exports.is_auth_tool_path = is_auth_tool_path;

  exports.is_br_auth_path = is_br_auth_path;

  exports.is_br_user_path = is_br_user_path;

  exports.is_br_push_notification_path = is_br_push_notification_path;

  exports.is_db_path = is_db_path;

  exports.error = error;

  exports.get_referenced_schemas = get_referenced_schemas;

  exports.write_response = write_response;

}).call(this);

//# sourceMappingURL=common-helpers.map
